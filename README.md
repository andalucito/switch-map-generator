# switch-map-generator
This is a small homebrew app that generates a scrollable randomly generated world map.

## Controls
- D-pad Left/Right to scroll the map.
- A to generate a new map.

# Building

## Dependencies
- DevKitPro's Nintendo Switch thingy
- raylib-nx

As long as you have the dependencies doing 'make' should work, but I'm not sure.
