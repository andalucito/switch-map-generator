#include <switch.h>
#define FNL_IMPL
#include <stdlib.h>
#include "raylib.h"
#include "FastNoiseLite.h"


int main(void) {
    const int screenWidth = 1280;
    const int screenHeight = 720;

    int pos_x = 0;

    srand(3);

    fnl_state noise = fnlCreateState(3);
    noise.noise_type = FNL_NOISE_PERLIN;
    noise.frequency = 0.02f;
    noise.seed = 222;
    noise.octaves = 8;
    noise.lacunarity = 2.0f;
    noise.gain = 0.7f;
    noise.fractal_type = FNL_FRACTAL_FBM;

    InitWindow(screenWidth, screenHeight, "Sui test");

    Image img = GenImageColor(1280, 720, BROWN);
    ImageFormat(&img, PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
    Texture2D wt = LoadTextureFromImage(img);
    Color *pixels = LoadImageColors(img);

    for (float y = 0; y < screenHeight; y++) {
        for (float x = 0; x < screenWidth; x++) {
            
            int i = x + y * screenWidth;


            float cylinder_x = cosf(x / 1280.0f * DEG2RAD * 360.0f) * 0.03f * screenWidth;
            float cylinder_z = sinf(x / 1280.0f * DEG2RAD * 360.0f) * 0.03f * screenWidth;


            float altitude = fnlGetNoise3D(&noise, cylinder_x, y * 0.03f * 8, cylinder_z);
            float temperature = fnlGetNoise3D(&noise, cylinder_x + 7000.0f, y * 0.03f * 8, cylinder_z + 7000.0f) * 0.3;
                temperature += -abs(y - screenHeight/2) / 720.0f;
                temperature += 0.23f;
            float moisture = fnlGetNoise3D(&noise, cylinder_x - 7000.0f, y * 0.03f * 8, cylinder_z - 7000.0f);

            if (altitude > 0.05f) {
                // Grass
                pixels[i] = GREEN;
                if (temperature > 0.11f) {
                    pixels[i] = YELLOW;
                    if (moisture > 0.0f) {
                        pixels[i] = DARKGREEN;
                    }
                } else if (temperature < -0.11f) {
                    pixels[i] = WHITE;
                }
            } else {
                // Water
                pixels[i] = BLUE;
            }
            

        }
    }

    UpdateTexture(wt, pixels);

    // NOTE: Textures MUST be loaded after Window initialization (OpenGL context is required)
    //Texture2D texture = LoadTexture("romfs:/resources/raylib_logo.png");        // Texture loading
    //---------------------------------------------------------------------------------------

    PadState pad;
    padInitializeDefault(&pad);

    // Main game loop
    while (appletMainLoop()) {
        
        // Update
        padUpdate(&pad);

        u64 kDown = padGetButtonsDown(&pad);
        u64 kHeld = padGetButtons(&pad);
        u64 kUp = padGetButtonsUp(&pad);


        // A
        if (kDown & HidNpadButton_A) {
            noise.seed = rand();

            BeginDrawing();

            DrawText("Regenerating Map...", 19, 19, 40, BLACK);
            DrawText("Regenerating Map...", 16, 16, 40, WHITE);

            EndDrawing();
            
            for (float y = 0; y < screenHeight; y++) {
                for (float x = 0; x < screenWidth; x++) {
                    
                    int i = x + y * screenWidth;


                    float cylinder_x = cosf(x / 1280.0f * DEG2RAD * 360.0f) * 0.03f * screenWidth;
                    float cylinder_z = sinf(x / 1280.0f * DEG2RAD * 360.0f) * 0.03f * screenWidth;


                    float altitude = fnlGetNoise3D(&noise, cylinder_x, y * 0.03f * 8, cylinder_z);
                    float temperature = fnlGetNoise3D(&noise, cylinder_x + 7000.0f, y * 0.03f * 8, cylinder_z + 7000.0f) * 0.3;
                        temperature += -abs(y - screenHeight/2) / 720.0f;
                        temperature += 0.23f;
                    float moisture = fnlGetNoise3D(&noise, cylinder_x - 7000.0f, y * 0.03f * 8, cylinder_z - 7000.0f);

                    if (altitude > 0.05f) {
                        // Grass
                        pixels[i] = GREEN;
                        if (temperature > 0.11f) {
                            pixels[i] = YELLOW;
                            if (moisture > 0.0f) {
                                pixels[i] = DARKGREEN;
                            }
                        } else if (temperature < -0.11f) {
                            pixels[i] = WHITE;
                        }
                    } else {
                        // Water
                        pixels[i] = BLUE;
                    }
                    

                }
            }
            UpdateTexture(wt, pixels);
        } // [A] pressed

        // +
        if (kDown & HidNpadButton_Plus) {
            break;
        }

        // <-
        if (kHeld & HidNpadButton_Left) {
            pos_x-=16;
        }

        // ->
        if (kHeld & HidNpadButton_Right) {
            pos_x+=16;
        }


        // Keep view on the maps
        if (pos_x < -screenWidth) {
            pos_x += screenWidth;
        }
        if (pos_x > screenWidth) {
            pos_x -= screenWidth;
        }


        // Draw
        BeginDrawing();

            ClearBackground(BLACK);

            DrawTexture(wt, -screenWidth - pos_x, 0, WHITE);
            DrawTexture(wt, 0 - pos_x, 0, WHITE);
            DrawTexture(wt, screenWidth - pos_x, 0, WHITE);

        EndDrawing();
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(wt);       // Texture unloading
    UnloadImage(img);

    

    CloseWindow();                // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    consoleExit(NULL);
    return 0;

}
